FROM golang:1.15-alpine
RUN apk update && apk add gcc git 
WORKDIR /go/src/stock_imported_backend
COPY . /go/src/stock_imported_backend
RUN go get -d -v
RUN go build -o main .

FROM alpine:latest  
RUN apk --no-cache add ca-certificates
RUN apk update \
	&& apk add tzdata \
	&& cp /usr/share/zoneinfo/Asia/Bangkok /etc/localtime \
	&& echo "Asia/Bangkok" >  /etc/timezone \
	&& echo "Asia/Bangkok" >  /etc/TZ \
	&& unset TZ

WORKDIR /go/src/stock_imported_backend
COPY --from=0 /go/src/stock_imported_backend/main .
CMD ["./main"]  

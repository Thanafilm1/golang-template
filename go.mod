module store_beef_backend

go 1.15

require (
	github.com/boombuler/barcode v1.0.0
	github.com/chilts/sid v0.0.0-20190607042430-660e94789ec9
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/schema v1.2.0
	github.com/matoous/go-nanoid v1.5.0
	github.com/matoous/go-nanoid/v2 v2.0.0
	github.com/rs/cors v1.7.0
	golang.org/x/crypto v0.0.0-20201217014255-9d1352758620
)

package database

import (
	"fmt"
	"store_beef_backend/pkg/model"
)
func (dbConf *DBConf) ManageShabuTable(shabu model.ShabuTable) error {
	str := `UPDATE shabu_table SET persons = %d,adult =  %d,child = %d,check_in_at = %d,name= '%s',phone= %d,start_at= %d,end_at= %d,status_id= %d WHERE id = %d`
	sql := fmt.Sprintf(str,shabu.Persons,shabu.Adult,shabu.Child,shabu.CheckInAt,shabu.Name,shabu.Phone,shabu.StartAt,shabu.EndAt,shabu.StatusId,shabu.ID)

	insFrom, err := db.Query(sql)
	if err != nil {
		return err
	}
	defer insFrom.Close()

	return err
}
func (dbConf *DBConf) CreateShabuTableSetting(name string,max int) error {
	str := `INSERT INTO shabu_table (table_name,max) VALUES ('%s',%d)`
	sql := fmt.Sprintf(str,name,max)

	insFrom, err := db.Query(sql)
	if err != nil {
		return err
	}
	defer insFrom.Close()

	return err
}

func (dbConf *DBConf) UpdateShabuTableSetting(name string,max int,id int) error {
	str := `UPDATE shabu_table SET table_name = '%s',max = %d WHERE id = %d`
	sql := fmt.Sprintf(str,name,max,id)

	insFrom, err := db.Query(sql)
	if err != nil {
		return err
	}
	defer insFrom.Close()
	return nil
}

func (dbConf *DBConf) DeleteShabuTableSetting(id int) error {
	str := `DELETE FROM shabu_table WHERE id = %d`
	sql := fmt.Sprintf(str, id)

	insFrom, err := db.Query(sql)
	if err != nil {
		return err
	}
	defer insFrom.Close()
	return nil
}


func (dbConf *DBConf) CreateShabuTableStatus(statusTB model.ShabuStatusTable) error {
	str := `INSERT INTO shabu_status_table (value) VALUES ('%s')`
	sql := fmt.Sprintf(str,statusTB.Value)

	insFrom, err := db.Query(sql)
	if err != nil {
		return err
	}
	defer insFrom.Close()

	return err
}

func (dbConf *DBConf) UpdateShabuTableStatus(statusTB model.ShabuStatusTable) error {
	str := `UPDATE shabu_status_table SET value = '%s' WHERE id = %d`
	sql := fmt.Sprintf(str,statusTB.Value,statusTB.ID)

	insFrom, err := db.Query(sql)
	if err != nil {
		return err
	}
	defer insFrom.Close()
	return nil
}

func (dbConf *DBConf) DeleteShabuTableStatus(id int) error {
	str := `DELETE FROM shabu_table WHERE id = %d`
	sql := fmt.Sprintf(str, id)

	insFrom, err := db.Query(sql)
	if err != nil {
		return err
	}
	defer insFrom.Close()
	return nil
}

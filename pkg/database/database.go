package database

import (
	"database/sql"
	"fmt"
	"store_beef_backend/pkg/model"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

// DBConf ...
type DBConf struct {
	Host     string
	Port     string
	User     string
	Pass     string
	Database string
}

var db *sql.DB

func dsn(host string, port string, user string, pass string, dbName string) string {
	return fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", user, pass, host, port, dbName)
}

// GetDBConfig ....
func GetDBConfig(host string, port string, user string, pass string, dbName string) StoreBeefInterface {
	return &DBConf{
		Host:     host,
		Port:     port,
		User:     user,
		Pass:     pass,
		Database: dbName,
	}
}

// NewConnectionDB ...
func (dbConf *DBConf) NewConnectionDB() error {
	var err error
	dsn := dsn(dbConf.Host, dbConf.Port, dbConf.User, dbConf.Pass, dbConf.Database)

	db, err = sql.Open("mysql", dsn)
	if err != nil {
		return err
	}

	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	return nil
}

// Ping ...
func (dbConf *DBConf) Ping() error {
	return db.Ping()
}

// Close ...
func (dbConf *DBConf) Close() error {
	return db.Close()
}

// StoreBeefInterface ...
type StoreBeefInterface interface {
	NewConnectionDB() error
	Ping() error
	Close() error

	// Users
	CreateUser(user model.User) error
	GetUser(username string) (model.User, error)

	//product
	ListProduct(keyword string, type_id string) ([]model.Product, error)
	GetProduct(barcode string) (model.Product, error)
	MapProduct(mp model.MapProduct) error

	//import_product
	CreateImportProduct(improduct model.ImportProduct) error
	UpdateImportProduct(improduct model.ImportProduct) error
	DeleteImportProduct(id string) error
	CreatePictureProduct(pp model.PictureProduct) error

	//product_type_setting
	CreateProductType(pt model.ProductType) error
	UpdateProductType(pt model.ProductType) error
	DeleteProductType(id int) error

	//shabu_table
	ManageShabuTable(shabu model.ShabuTable) error

	
	//shabu_table_setting
	CreateShabuTableSetting(name string, max int) error
	UpdateShabuTableSetting(name string, max int, id int) error
	DeleteShabuTableSetting(id int) error
	
	CreateShabuTableStatus(statusTB model.ShabuStatusTable) error
	UpdateShabuTableStatus(statusTB model.ShabuStatusTable) error
	DeleteShabuTableStatus(id int) error
}

package database

import (
	"fmt"
	"store_beef_backend/pkg/model"
)

func (dbConf *DBConf) CreateProductType(pt model.ProductType) error {
	str := `INSERT INTO product_type (value) VALUES ('%s')`
	sql := fmt.Sprintf(str,pt.Value)

	insFrom, err := db.Query(sql)
	if err != nil {
		return err
	}
	defer insFrom.Close()

	return err
}

func (dbConf *DBConf) UpdateProductType(pt model.ProductType) error {
	str := `UPDATE product_type SET value = '%s' WHERE id = %d`
	sql := fmt.Sprintf(str,pt.Value,pt.ID)

	insFrom, err := db.Query(sql)
	if err != nil {
		return err
	}
	defer insFrom.Close()
	return nil
}

func (dbConf *DBConf) DeleteProductType(id int) error {
	str := `DELETE FROM product_type WHERE id = %d`
	sql := fmt.Sprintf(str, id)

	insFrom, err := db.Query(sql)
	if err != nil {
		return err
	}
	defer insFrom.Close()
	return nil
}

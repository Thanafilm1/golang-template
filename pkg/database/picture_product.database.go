package database

import (
	"fmt"
	"store_beef_backend/pkg/model"
)

func (dbConf *DBConf) CreatePictureProduct(pp model.PictureProduct) error {
	str := `INSERT INTO picture_product (product_id,path) VALUES ('%s','%s')`
	sql := fmt.Sprintf(str,pp.ProductID,pp.Path)

	insFrom, err := db.Query(sql)
	if err != nil {
		return err
	}
	defer insFrom.Close()

	return err
}

// func (dbConf *DBConf) UpdateProductType(pt model.ProductType) error {
// 	str := `UPDATE product_type SET value = '%s' WHERE id = %d`
// 	sql := fmt.Sprintf(str,pt.Value,pt.ID)

// 	insFrom, err := db.Query(sql)
// 	if err != nil {
// 		return err
// 	}
// 	defer insFrom.Close()
// 	return nil
// }

// func (dbConf *DBConf) DeleteProductType(id int) error {
// 	str := `DELETE FROM product_type WHERE id = %d`
// 	sql := fmt.Sprintf(str, id)

// 	insFrom, err := db.Query(sql)
// 	if err != nil {
// 		return err
// 	}
// 	defer insFrom.Close()
// 	return nil
// }

package database

import (
	"fmt"
	"store_beef_backend/pkg/model"
	// "strconv"
)

func (dbConf *DBConf) ListProduct(keyword string, type_id string) ([]model.Product, error) {
	var product []model.Product
	sqlStr := `SELECT * FROM (SELECT m.barcode_key,m.type_product_id,im.name,im.description,im.price,im.barcode,im.qrcode,im.amount,im.type_id,pt.value FROM import_product as im
		INNER JOIN map_product as m on im.product_id = m.product_id
		INNER JOIN product_type as pt on im.type_id = pt.id
		union all 
		SELECT m.barcode_key,m.type_product_id,mp.name,mp.description,mp.price,mp.barcode,mp.qrcode,mp.amount,mp.type_id,pt.value FROM meat_product
		as mp
		INNER JOIN map_product as m on mp.product_id = m .product_id
		INNER JOIN product_type as pt on mp.type_id = pt.id) as list`

	if type_id != "" && keyword != "" {
		sqlStr += ` WHERE list.type_product_id ='` + type_id + ` 'AND list.name LIKE '%` + keyword + `%'  `

	} else {
		if keyword != "" {
			sqlStr += ` WHERE (list.name LIKE '%` + keyword + `%')`
		}
		if type_id != "" {
			sqlStr += ` WHERE type_product_id = ` + type_id + ` `
		}
	}

	selFrom, err := db.Query(sqlStr)
	if err != nil {
		panic(err.Error())
	}

	for selFrom.Next() {
		var m model.Product
		err = selFrom.Scan(&m.Key, &m.TypeProductID, &m.Name, &m.Description, &m.Price, &m.Barcode, &m.QRcode, &m.Amount, &m.TypeID, &m.TypeDescpription)
		if err != nil {
			panic(err.Error())
		}

		product = append(product, m)
	}
	defer selFrom.Close()
	return product, err
}

func (dbConf *DBConf) CreateImportProduct(improduct model.ImportProduct) error {
	str := `INSERT INTO 
		import_product(product_id, name, description, price, mfd_at, exp_at, barcode, qrcode, amount, type_id) 
		VALUES ('%s','%s','%s',%f, %d, '%d', '%s', '%s', %d, %d)`
	sql := fmt.Sprintf(str, improduct.ProductID, improduct.Name, improduct.Description, improduct.Price, improduct.MFDAt, improduct.EXPAt, improduct.Barcode, improduct.QRcode, improduct.Amount, improduct.TypeID)

	insFrom, err := db.Query(sql)
	if err != nil {
		return err
	}
	defer insFrom.Close()

	return err
}

func (dbConf *DBConf) UpdateImportProduct(improduct model.ImportProduct) error {
	str := `UPDATE import_product SET name = '%s', description = '%s', price = %f, mfd_at = %d,
			exp_at = '%d', barcode = '%s', qrcode =' %s', amount = %d, type_id = %d
			WHERE product_id = '%s'`
	sql := fmt.Sprintf(str, improduct.Name, improduct.Description, improduct.Price, improduct.MFDAt, improduct.EXPAt, improduct.Barcode,
		improduct.QRcode, improduct.Amount, improduct.TypeID, improduct.ProductID)

	insFrom, err := db.Query(sql)
	if err != nil {
		return err
	}
	defer insFrom.Close()
	return nil
}

func (dbConf *DBConf) DeleteImportProduct(id string) error {
	str := `DELETE FROM import_product WHERE product_id = '%s'`
	sql := fmt.Sprintf(str, id)

	insFrom, err := db.Query(sql)
	if err != nil {
		return err
	}
	defer insFrom.Close()
	return nil
}

func (dbConf *DBConf) MapProduct(mp model.MapProduct) error {
	str := `INSERT INTO 
		map_product(barcode,product_id,type_product_id) 
		VALUES ('%s','%s',%d)`
	sql := fmt.Sprintf(str, mp.Barcode, mp.ProductID, mp.TypeProductID)

	insFrom, err := db.Query(sql)
	if err != nil {
		return err
	}
	defer insFrom.Close()

	return err
}

func (dbConf *DBConf) GetProduct(barcode string) (model.Product, error) {
	var p model.Product
	sqlStr := `SELECT * FROM (SELECT m.barcode_key,m.type_product_id,im.name,im.description,im.price,im.barcode,im.qrcode,im.amount,im.type_id,pt.value FROM import_product as im
		INNER JOIN map_product as m on im.product_id = m.product_id
		INNER JOIN product_type as pt on im.type_id = pt.id
		union all 
		SELECT m.barcode_key,m.type_product_id,mp.name,mp.description,mp.price,mp.barcode,mp.qrcode,mp.amount,mp.type_id,pt.value FROM meat_product
		as mp
		INNER JOIN map_product as m on mp.product_id = m .product_id
		INNER JOIN product_type as pt on mp.type_id = pt.id) as list
		WHERE list.barcode = '%s'`
	sql := fmt.Sprintf(sqlStr, barcode)
	selFrom, err := db.Query(sql)
	if err != nil {
		panic(err.Error())
	}

	for selFrom.Next() {
		err = selFrom.Scan(&p.Key, &p.TypeProductID, &p.Name, &p.Description, &p.Price, &p.Barcode, &p.QRcode, &p.Amount, &p.TypeID, &p.TypeDescpription)
		if err != nil {
			panic(err.Error())
		}
	}
	defer selFrom.Close()
	return p, err
}
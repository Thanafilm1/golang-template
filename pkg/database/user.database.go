package database

import (
	"fmt"
	"store_beef_backend/pkg/model"
)

// CreateUser ...
func (dbConf *DBConf) CreateUser(user model.User) error {
	str := `INSERT INTO user(username, password, first_name, last_name, phone, role_id, created_at, updated_at) VALUES ('%s','%s','%s','%s','%s', %d, %d, %d)`
	sql := fmt.Sprintf(str, user.Username, user.Password, user.FirstName, user.LastName, user.Phone, user.RoleID, user.CreatedAt, user.UpdatedAt)

	insFrom, err := db.Query(sql)
	if err != nil {
		return err
	}
	defer insFrom.Close()

	return err
}

// GetUser ...
func (dbConf *DBConf) GetUser(username string) (model.User, error) {
	var users model.User
	sqlStr := `SELECT u.id, u.username, u.password, u.first_name, u.last_name, u.phone, u.role_id, g.role, u.created_at, u.updated_at 
		FROM user AS u
		LEFT JOIN group_user AS g ON u.role_id = g.id
		WHERE u.username = '%s'`
	sql := fmt.Sprintf(sqlStr, username)
	selFrom, err := db.Query(sql)
	if err != nil {
		panic(err.Error())
	}

	for selFrom.Next() {
		err = selFrom.Scan(&users.ID, &users.Username, &users.Password, &users.FirstName, &users.LastName, &users.Phone, &users.RoleID, &users.Role, &users.CreatedAt, &users.UpdatedAt)
		if err != nil {
			panic(err.Error())
		}
	}
	defer selFrom.Close()
	return users, err
}

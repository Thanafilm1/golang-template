package rest

import (
	"net/http"
	"store_beef_backend/pkg/model"

	"strconv"

	"github.com/gorilla/mux"
)

func ProductType(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, _ := strconv.Atoi(vars["id"])
	switch r.Method {
	case "POST":
		var product_type model.ProductType

		_ = r.ParseForm()
		err := decoder.Decode(&product_type, r.PostForm)
		if err != nil {
			rw.BadRequest(w, err.Error())
			return
		}

		err = service.CreateProductTypeService(product_type)
		if err != nil {
			rw.BadRequest(w, err.Error())
			return
		}
		rw.Created(w, "product_type_imported")

	case "PUT":
		var product_type model.ProductType
		_ = r.ParseForm()
		err := decoder.Decode(&product_type, r.PostForm)
		if err != nil {
			rw.BadRequest(w, err.Error())
			return
		}

		product_type.ID = id
		err = service.UpdateProductTypeService(product_type)
		if err != nil {
			rw.BadRequest(w, err.Error())
			return
		}
		rw.Created(w, "product_type_updated")

	case "DELETE":
		err := service.DeleteProductTypeService(id)
		if err != nil {
			rw.BadRequest(w, err.Error())
			return
		}
		rw.Created(w, "product_type_delete")
	}
}

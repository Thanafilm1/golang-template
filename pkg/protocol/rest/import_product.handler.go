package rest

import (
	"net/http"
	"store_beef_backend/pkg/model"
	"store_beef_backend/pkg/util"

	// "strconv"
	"github.com/gorilla/mux"
)

func ImportProduct(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]
	switch r.Method {
	case "POST":
		var import_product model.ImportProduct
		var mp model.MapProduct

		_ = r.ParseForm()
		err := decoder.Decode(&import_product, r.PostForm)
		if err != nil {
			rw.BadRequest(w, err.Error())
			return
		}

		err = service.CreateImportProductService(import_product)
		if err != nil {
			rw.BadRequest(w, err.Error())
			return
		}
		mp.ProductID = import_product.ProductID
		mp.TypeProductID = import_product.TypeID
		err = service.MapProductService(mp)
		if err != nil {
			rw.BadRequest(w, err.Error())
			return
		}
		rw.Created(w, "product_imported")

	case "PUT":
		var import_product model.ImportProduct
		_ = r.ParseForm()
		err := decoder.Decode(&import_product, r.PostForm)
		if err != nil {
			rw.BadRequest(w, err.Error())
			return
		}

		import_product.ProductID = id
		err = service.UpdateImportProductService(import_product)
		if err != nil {
			rw.BadRequest(w, err.Error())
			return
		}
		rw.Created(w, "product_updated")

	case "DELETE":
		err := service.DeleteImportProductService(id)
		if err != nil {
			rw.BadRequest(w, err.Error())
			return
		}
		rw.Created(w, "product_delete")
	}
}

func ProductID(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		query := r.URL.Query()
		barcode_item := query.Get("barcode")
		c, err := service.GetProductService(barcode_item)
		if err != nil {
			rw.BadRequest(w, err.Error())
			return
		}
		rw.Success(w, "list product success", c)
	}
}
func ListProduct(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		var pagination model.Pagination

		query := r.URL.Query()
		pagination.Keyword = query.Get("keyword")
		pagination.Page, pagination.Limit = util.DefaultPageLimit(query.Get("page"), query.Get("limit"))
		type_id := query.Get("type_id")
		mp, pagination, err := service.ListProductService(pagination, type_id)
		if err != nil {
			rw.BadRequest(w, err.Error())
			return
		}

		rw.SuccessPagenation(w, "list  success", mp, pagination)
	}
}

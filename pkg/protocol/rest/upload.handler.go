package rest

import (
	"log"
	"net/http"
	"store_beef_backend/pkg/model"
)

func UploadProduct(w http.ResponseWriter, r *http.Request) {
	p_id := r.FormValue("product_id")
	switch r.Method {
	case "POST":
		err := r.ParseMultipartForm(100000)
		if err != nil {
			log.Println(err)
		}

		files := r.MultipartForm.File["files"]
		for i := range files {
			if files[i].Header["Content-Type"][0] != "image/png" && files[i].Header["Content-Type"][0] != "image/jpeg" {
				rw.BadRequest(w, "require file type .png or .jpg")
				return
			}
		}
		path, _ := service.UploadFileService(files)
		for _, p := range path {

			var pic model.PictureProduct
			pic.Path = p["path"]
			pic.ProductID = p_id
			err = service.CreatePictureProductService(pic)
			if err != nil {
				log.Println(err)
			}
		}
		rw.Created(w, "picture_uploaded")
	}
}

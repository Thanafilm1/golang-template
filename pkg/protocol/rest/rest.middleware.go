package rest

import (
	"log"
	"net/http"
	"store_beef_backend/pkg/util"
	"strings"

	"github.com/gorilla/mux"
)

func shearGroupUser(hightRole string) []string {
	var roles []string

	switch hightRole {
	case "personal":
		roles = []string{"personal", "admin"}

	case "admin":
		roles = []string{"admin"}
	}
	return roles
}

func checkRole(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

func middleware(roles []string) mux.MiddlewareFunc {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			header := strings.TrimSpace(strings.Replace(r.Header.Get("authorization"), "beef ", "", 1))
			if header == "" {
				rw.Unauthorized(w, "missing auth token")
				return
			}

			tk, err := util.JWTParse(header)
			if err != nil {
				rw.Unauthorized(w, err.Error())
				return
			}

			if !checkRole(roles, tk.Role) {
				rw.Unauthorized(w, "ไม่มีสิทธิ์ใช้งาน")
				return
			}

			log.Printf("%s: %s [%s]", r.Method, r.RequestURI, tk.Role)
			next.ServeHTTP(w, r)
		})
	}
}

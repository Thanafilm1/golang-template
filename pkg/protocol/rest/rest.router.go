package rest

import (
	"net/http"

	"github.com/gorilla/mux"
)

func router() *mux.Router {
	version := "/v1"
	storage := "/storage/"

	r := mux.NewRouter()
	r.HandleFunc("/", yourHandlerAdmin)
	r.PathPrefix(version + storage).Handler(http.StripPrefix(version+storage, http.FileServer(http.Dir("."+storage))))

	// Public
	pub := r.PathPrefix(version).Subrouter()
	a := r.PathPrefix(version).Subrouter()
	u := r.PathPrefix(version).Subrouter()

	a.Use(middleware(shearGroupUser("admin")))
	u.Use(middleware(shearGroupUser("personal")))

	// Auth
	pub.HandleFunc("/auth/login", Login)
	a.HandleFunc("/auth/register", Register)

	//product
	pub.HandleFunc("/product", ProductID).Methods("GET")
	pub.HandleFunc("/products", ListProduct).Methods("GET")

	//Import Product
	pub.HandleFunc("/imported/product", ImportProduct).Methods("POST")
	pub.HandleFunc("/imported/product/{id}", ImportProduct).Methods("PUT", "DELETE")
	pub.HandleFunc("/upload/product", UploadProduct).Methods("POST")

	//product_type_setting
	pub.HandleFunc("/product/type", ProductType).Methods("POST")
	pub.HandleFunc("/product/type/{id}", ProductType).Methods("PUT", "DELETE")

	//shabu_table
	pub.HandleFunc("/shabu/table", ShabuTable).Methods("POST")

	//shabu_table_setting
	pub.HandleFunc("/setting/shabu/table", ShabutableSetting).Methods("POST")
	pub.HandleFunc("/setting/shabu/table/{id}", ShabutableSetting).Methods("PUT", "DELETE")
	pub.HandleFunc("/setting/shabu/status", ShabutableStatus).Methods("POST")
	pub.HandleFunc("/setting/shabu/status/{id}", ShabutableStatus).Methods("PUT", "DELETE")

	return r
}

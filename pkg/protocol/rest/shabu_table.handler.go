package rest

import (
	"net/http"
	"store_beef_backend/pkg/model"

	"strconv"

	"github.com/gorilla/mux"
)

func ShabuTable(w http.ResponseWriter, r *http.Request) {
	var shabu model.ShabuTable

	switch r.Method {
	case "POST":
		_ = r.ParseForm()
		err := decoder.Decode(&shabu, r.PostForm)
		if err != nil {
			rw.BadRequest(w, err.Error())
			return
		}

		err = service.ManageShabuTableService(shabu)
		if err != nil {
			rw.BadRequest(w, err.Error())
			return
		}
		rw.Created(w, "shabu_table_updated")

	case "GET":

	}
}

func ShabutableStatus(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, _ := strconv.Atoi(vars["id"])
	switch r.Method {
	case "POST":
		var stb model.ShabuStatusTable

		_ = r.ParseForm()
		err := decoder.Decode(&stb, r.PostForm)
		if err != nil {
			rw.BadRequest(w, err.Error())
			return
		}

		err = service.CreateShabuTableStatusService(stb)
		if err != nil {
			rw.BadRequest(w, err.Error())
			return
		}
		rw.Created(w, "shabu_status_imported")

	case "PUT":
		var stb model.ShabuStatusTable
		_ = r.ParseForm()
		err := decoder.Decode(&stb, r.PostForm)
		if err != nil {
			rw.BadRequest(w, err.Error())
			return
		}

		stb.ID = id
		err = service.UpdateShabuTableStatusService(stb)
		if err != nil {
			rw.BadRequest(w, err.Error())
			return
		}
		rw.Created(w, "shabu_status_updated")

	case "DELETE":
		err := service.DeleteShabuTableStatusService(id)
		if err != nil {
			rw.BadRequest(w, err.Error())
			return
		}
		rw.Created(w, "shabu_status_delete")
	}
}

func ShabutableSetting(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, _ := strconv.Atoi(vars["id"])
	table_name := r.FormValue("table_name")
	max, _ := strconv.Atoi(r.FormValue("max"))
	switch r.Method {
	case "POST":
		var shabu model.ShabuTable
		_ = r.ParseForm()
		err := decoder.Decode(&shabu, r.PostForm)
		if err != nil {
			rw.BadRequest(w, err.Error())
			return
		}

		err = service.CreateShabuTableSettingService(table_name, max)
		if err != nil {
			rw.BadRequest(w, err.Error())
			return
		}
		rw.Created(w, "shabu_table_imported")

	case "PUT":
		var shabu model.ShabuTable
		_ = r.ParseForm()
		err := decoder.Decode(&shabu, r.PostForm)
		if err != nil {
			rw.BadRequest(w, err.Error())
			return
		}

		err = service.UpdateShabuTableSettingService(table_name, max, id)
		if err != nil {
			rw.BadRequest(w, err.Error())
			return
		}
		rw.Created(w, "shabu_table_updated")

	case "DELETE":
		err := service.DeleteShabuTableSettingService(id)
		if err != nil {
			rw.BadRequest(w, err.Error())
			return
		}
		rw.Created(w, "shabu_table_delete")
	}
}

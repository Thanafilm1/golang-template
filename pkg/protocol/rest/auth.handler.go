package rest

import (
	"net/http"
	"store_beef_backend/pkg/model"
)

// Login ...
func Login(w http.ResponseWriter, r *http.Request) {
	var user model.User

	_ = r.ParseForm()
	err := decoder.Decode(&user, r.PostForm)
	if err != nil {
		rw.BadRequest(w, err.Error())
		return
	}

	token, err := service.LoginService(user)
	if err != nil {
		rw.BadRequest(w, err.Error())
		return
	}

	rw.Success(w, "login success", token)
}

// Register ...
func Register(w http.ResponseWriter, r *http.Request) {
	var user model.User

	_ = r.ParseForm()
	err := decoder.Decode(&user, r.PostForm)
	if err != nil {
		rw.BadRequest(w, err.Error())
		return
	}

	err = service.RegisterService(user)
	if err != nil {
		rw.BadRequest(w, err.Error())
		return
	}

	rw.Created(w, "created user success")
}

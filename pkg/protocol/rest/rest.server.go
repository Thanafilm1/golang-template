package rest

import (
	"log"
	"net/http"
	v1 "store_beef_backend/pkg/service/v1"
	"store_beef_backend/pkg/util"

	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"github.com/rs/cors"
)

var (
	service v1.ServiceInterface
	decoder *schema.Decoder
	rw      util.BodyResponse
)

func enableCors(r *mux.Router) http.Handler {
	c := cors.New(cors.Options{
		AllowedHeaders:   []string{"*"},
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowCredentials: true,
		Debug:            false,
	})
	return c.Handler(r)
}

func yourHandlerAdmin(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Beef Store Admin!\n"))
}

func yourHandlerUser(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Beef Store User!\n"))
}

// RunServer is starting rest server
func RunServer(addr string) error {
	r := router()
	handler := enableCors(r)

	service = v1.InitailService()
	service.StartingService()

	decoder = schema.NewDecoder()

	rw = util.InitialResponse()
	// log.SetOutput(ioutil.Discard)
	log.Printf("Server rest is starting on %s", "http://localhost:"+addr)
	err := http.ListenAndServe(":"+addr, handler)
	if err != nil {
		return err
	}
	return nil
}

package v1

import (
	"fmt"
	"github.com/chilts/sid"
	"io"
	"mime/multipart"
	"os"
	"strings"
)

func (app *App) UploadFileService(fhs []*multipart.FileHeader) ([]map[string]string, error) {

	var fileName string
	var paths []map[string]string
	for _, fh := range fhs {
		file, err := fh.Open()
		if err != nil {
			return paths, err
		}
		fileType := strings.SplitN(fh.Header["Content-Type"][0], "/", 2)
		dateStr := fmt.Sprintf("/storage/product/image")
		fileName = fmt.Sprintf("%s/%s.%s", dateStr, sid.IdBase64(), fileType[1])
		err = createDir(dateStr)
		if err != nil {
			panic(err)
		}
		f, err := os.OpenFile("."+fileName, os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			panic(err)
		}
		defer f.Close()
		_, _ = io.Copy(f, file)
		objFile := map[string]string{
			"path": fileName,
			"type": fileType[0],
		}
		paths = append(paths, objFile)
	}
	return paths, nil
}

func createDir(pathStr string) error {
	if _, err := os.Stat("." + pathStr); os.IsNotExist(err) {
		errDir := os.MkdirAll("."+pathStr, 0755)
		if errDir != nil {
			return errDir
		}
	}
	return nil
}

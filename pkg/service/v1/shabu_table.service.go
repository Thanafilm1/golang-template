package v1

import (
	"store_beef_backend/pkg/model"
	"time"
)

func (app *App) ManageShabuTableService(shabu model.ShabuTable) error {
	now := time.Now().Unix()
	shabu.CheckInAt = now
	shabu.StartAt = 0
	shabu.EndAt = 0
	if shabu.StatusId == 0 {
		shabu.StartAt = 0
		err := app.DB.ManageShabuTable(shabu)
		if err != nil {
			return err
		}

		return err
	} else {
		shabu.StartAt = now
		err := app.DB.ManageShabuTable(shabu)
		if err != nil {
			return err
		}

		return err
	}

}
func (app *App) CreateShabuTableStatusService(stb model.ShabuStatusTable) error {
	err := app.DB.CreateShabuTableStatus(stb)
	if err != nil {
		return err
	}
	return nil
}
func (app *App) UpdateShabuTableStatusService(stb model.ShabuStatusTable) error {
	err := app.DB.UpdateShabuTableStatus(stb)
	if err != nil {
		return err
	}

	return err
}
func (app *App) DeleteShabuTableStatusService(id int) error {
	err := app.DB.DeleteProductType(id)
	if err != nil {
		return err
	}

	return err
}

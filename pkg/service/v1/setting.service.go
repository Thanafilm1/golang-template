package v1

import (
	"store_beef_backend/pkg/model"
)

func (app *App) CreateShabuTableSettingService(name string, max int) error {

	err := app.DB.CreateShabuTableSetting(name, max)
	if err != nil {
		return err
	}
	return nil
}
func (app *App) UpdateShabuTableSettingService(name string, max int, id int) error {
	err := app.DB.UpdateShabuTableSetting(name, max, id)
	if err != nil {
		return err
	}

	return err
}
func (app *App) DeleteShabuTableSettingService(id int) error {
	err := app.DB.DeleteShabuTableSetting(id)
	if err != nil {
		return err
	}

	return err
}

////////////////////////////

func (app *App) CreateProductTypeService(pt model.ProductType) error {
	err := app.DB.CreateProductType(pt)
	if err != nil {
		return err
	}
	return nil
}
func (app *App) UpdateProductTypeService(pt model.ProductType) error {
	err := app.DB.UpdateProductType(pt)
	if err != nil {
		return err
	}

	return err
}
func (app *App) DeleteProductTypeService(id int) error {
	err := app.DB.DeleteProductType(id)
	if err != nil {
		return err
	}

	return err
}

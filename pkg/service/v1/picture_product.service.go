package v1

import (
	"store_beef_backend/pkg/model"
)

func (app *App) CreatePictureProductService(pp model.PictureProduct) error {
	err := app.DB.CreatePictureProduct(pp)
	if err != nil {
		return err
	}
	return nil
}

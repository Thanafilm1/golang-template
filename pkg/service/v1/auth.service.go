package v1

import (
	"log"
	"store_beef_backend/pkg/model"
	"store_beef_backend/pkg/util"
	"time"
)

// LoginService ...
func (app *App) LoginService(user model.User) (model.Login, error) {
	var login model.Login

	users, err := app.DB.GetUser(user.Username)
	if err != nil {
		return login, err
	}

	checkPass := util.CheckPasswordHash(user.Password, users.Password)
	if !checkPass {
		return login, err
	}

	expiresAt := time.Now().Add(time.Hour * 300).Unix()

	tk, err := util.JwtGenarate(users, expiresAt)
	if err != nil {
		return login, err
	}

	login.User = users
	login.Token = tk

	return login, nil
}

// RegisterService ...
func (app *App) RegisterService(user model.User) error {
	user.Password, _ = util.HashPassword(user.Password)

	err := app.DB.CreateUser(user)
	if err != nil {
		return err
	}

	log.Println(user)
	return nil
}

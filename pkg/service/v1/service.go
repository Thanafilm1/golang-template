package v1

import (
	"log"
	"mime/multipart"
	"os"
	"store_beef_backend/pkg/database"
	"store_beef_backend/pkg/model"
)

// App ..
type App struct {
	DB database.StoreBeefInterface
}

// ServiceInterface ...
type ServiceInterface interface {
	StartingService()

	// Auth Service
	RegisterService(user model.User) error
	LoginService(user model.User) (model.Login, error)

	//product
	ListProductService(pagenetion model.Pagination, type_id string) ([]model.Product, model.Pagination, error)
	GetProductService(barcode string) (model.Product, error)
	MapProductService(mp model.MapProduct) error

	//Import Product
	CreateImportProductService(improduct model.ImportProduct) error
	UpdateImportProductService(improduct model.ImportProduct) error
	DeleteImportProductService(id string) error
	CreatePictureProductService(pp model.PictureProduct) error

	//product_type_setting
	CreateProductTypeService(pt model.ProductType) error
	UpdateProductTypeService(pt model.ProductType) error
	DeleteProductTypeService(id int) error

	//shabu_table
	ManageShabuTableService(shabu model.ShabuTable) error

	//shabu_table_setting
	CreateShabuTableSettingService(name string, max int) error
	UpdateShabuTableSettingService(name string, max int, id int) error
	DeleteShabuTableSettingService(id int) error

	CreateShabuTableStatusService(stb model.ShabuStatusTable) error
	UpdateShabuTableStatusService(stb model.ShabuStatusTable) error
	DeleteShabuTableStatusService(id int) error

	// upload
	UploadFileService(fhs []*multipart.FileHeader) ([]map[string]string, error)
}

// InitailService ...
func InitailService() ServiceInterface {
	return &App{
		DB: database.GetDBConfig(os.Getenv("MYSQL_HOST"), os.Getenv("MYSQL_PORT"), os.Getenv("MYSQL_USER"), os.Getenv("MYSQL_PASS"), os.Getenv("MYSQL_DATABASE")),
	}
}

// StartingService ...
func (app *App) StartingService() {
	err := app.DB.NewConnectionDB()
	if err != nil {
		log.Println(err)
		log.Println("Create connection database fail")
	}
	log.Println("Create connection database success")

	err = app.DB.Ping()
	if err != nil {
		log.Println(err)
		log.Println("Ping database fail")
	}
	log.Println("Ping database ok")
}

package v1

import (
	"store_beef_backend/pkg/model"
	"store_beef_backend/pkg/util"
	"strconv"
	"time"
)

func (app *App) CreateImportProductService(improduct model.ImportProduct) error {
	err := app.DB.CreateImportProduct(improduct)
	if err != nil {
		return err
	}
	return nil
}
func (app *App) UpdateImportProductService(improduct model.ImportProduct) error {
	err := app.DB.UpdateImportProduct(improduct)
	if err != nil {
		return err
	}

	return err
}
func (app *App) DeleteImportProductService(id string) error {
	err := app.DB.DeleteImportProduct(id)
	if err != nil {
		return err
	}

	return err
}

func (app *App) MapProductService(mp model.MapProduct) error {
	now := time.Now().Unix()
	bar := int(now)
	mp.Barcode = strconv.Itoa(bar)
	err := app.DB.MapProduct(mp)
	if err != nil {
		return err
	}
	return nil
}

func (app *App) ListProductService(pagenetion model.Pagination, type_id string) ([]model.Product, model.Pagination, error) {
	var m []model.Product

	m, err := app.DB.ListProduct(pagenetion.Keyword, type_id)
	if err != nil {
		return m, pagenetion, err
	}

	begin, end, pagenetion := util.GeneratePagenetion(len(m), pagenetion)
	m = m[begin:end]

	return m, pagenetion, nil
}
func (app *App) GetProductService(barcode string) (model.Product, error) {
	return app.DB.GetProduct(barcode)
}

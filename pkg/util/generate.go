package util

import (
	"image/png"
	"os"

	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/qr"
	gonanoid "github.com/matoous/go-nanoid"
)

// GetUUID ...
func GetUUID() (string, error) {
	alphabet := "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_"
	length := 30

	uuid, err := gonanoid.Generate(alphabet, length)
	if err != nil {
		return "", err
	}

	return uuid, nil
}

// GetBarCode ...
func GetBarCode() {
	// Create the barcode
	qrCode, _ := qr.Encode("Hello World", qr.M, qr.Auto)

	// Scale the barcode to 200x200 pixels
	qrCode, _ = barcode.Scale(qrCode, 200, 200)

	// create the output file
	file, _ := os.Create("qrcode.png")
	defer file.Close()

	// encode the barcode as png
	png.Encode(file, qrCode)
}

package util

import (
	"math"
	"store_beef_backend/pkg/model"
	"strconv"
)

// GeneratePagenetion ...
func GeneratePagenetion(lenArr int, pagenetion model.Pagination) (int, int, model.Pagination) {
	begin := (pagenetion.Page - 1) * pagenetion.Limit
	end := pagenetion.Page * pagenetion.Limit

	pagenetion.Total = lenArr
	pagenetion.Pages = int(math.Ceil(float64(lenArr) / float64(pagenetion.Limit)))
	pagenetion.Offset = (pagenetion.Page - 1) * pagenetion.Limit

	if lenArr < end {
		end = lenArr
	}

	return begin, end, pagenetion
}

// DefaultPageLimit ...
func DefaultPageLimit(page string, limit string) (int, int) {
	p, _ := strconv.Atoi(page)
	l, _ := strconv.Atoi(limit)

	if p <= 0 {
		p = 1
	}
	if l <= 0 {
		l = 10
	}
	return p, l
}

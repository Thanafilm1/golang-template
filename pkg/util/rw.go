package util

import (
	"encoding/json"
	"net/http"
	"store_beef_backend/pkg/model"
)

// Response ...
type Response struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

// ResponseData ...
type ResponseData struct {
	Status  string      `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

// ResponseDataList ...
type ResponseDataList struct {
	Status     string           `json:"status"`
	Message    string           `json:"message"`
	Result     Result           `json:"result"`
	Pagenation model.Pagination `json:"pagenetion"`
}

type Result struct {
	Data interface{} `json:"data"`
}

// StatusConf ...
type StatusConf struct {
	OK   string
	FAIL string
}

// BodyResponse ...
type BodyResponse interface {
	// OK
	Created(w http.ResponseWriter, msg string)
	Success(w http.ResponseWriter, msg string, data interface{})
	SuccessPagenation(w http.ResponseWriter, msg string, data interface{}, pagenation model.Pagination)

	// FAIL
	BadRequest(w http.ResponseWriter, msg string)
	Unauthorized(w http.ResponseWriter, msg string)
}

// InitialResponse ...
func InitialResponse() BodyResponse {
	return &StatusConf{
		OK:   "OK",
		FAIL: "FAIL",
	}
}

func jsonResponse(w http.ResponseWriter, code int, data interface{}) {
	resp, err := json.Marshal(data)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)

	w.Write(resp)
}

// Created 201
func (conf *StatusConf) Created(w http.ResponseWriter, msg string) {
	resp := Response{
		Status:  conf.OK,
		Message: msg,
	}
	jsonResponse(w, http.StatusCreated, resp)
}

// Success 200
func (conf *StatusConf) Success(w http.ResponseWriter, msg string, data interface{}) {
	resp := ResponseData{
		Status:  conf.OK,
		Message: msg,
		Data:    data,
	}
	jsonResponse(w, http.StatusOK, resp)
}

// SuccessPagenation 200
func (conf *StatusConf) SuccessPagenation(w http.ResponseWriter, msg string, data interface{}, pagenation model.Pagination) {
	resp := ResponseDataList{
		Status:     conf.OK,
		Message:    msg,
		Result:     Result{data},
		Pagenation: pagenation,
	}
	jsonResponse(w, http.StatusOK, resp)
}

// BadRequest 400
func (conf *StatusConf) BadRequest(w http.ResponseWriter, msg string) {
	resp := Response{
		Status:  conf.FAIL,
		Message: msg,
	}
	jsonResponse(w, http.StatusBadRequest, resp)
}

// Unauthorized 401
func (conf *StatusConf) Unauthorized(w http.ResponseWriter, msg string) {
	resp := Response{
		Status:  conf.FAIL,
		Message: msg,
	}
	jsonResponse(w, http.StatusUnauthorized, resp)
}

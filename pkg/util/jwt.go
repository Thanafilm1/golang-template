package util

import (
	"store_beef_backend/pkg/model"

	"github.com/dgrijalva/jwt-go"
)

// JwtGenarate ...
func JwtGenarate(user model.User, expiresAt int64) (string, error) {
	tk := &model.Token{
		ID:        user.ID,
		Username:  user.Username,
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Phone:     user.Phone,
		RoleID:    user.RoleID,
		Role:      user.Role,
		StandardClaims: &jwt.StandardClaims{
			ExpiresAt: expiresAt,
		},
	}
	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)
	tokenStr, err := token.SignedString([]byte("secret"))
	return tokenStr, err
}

// JWTParse ...
func JWTParse(header string) (*model.Token, error) {
	var tk = &model.Token{}
	_, err := jwt.ParseWithClaims(header, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte("secret"), nil
	})

	return tk, err
}

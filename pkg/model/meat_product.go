package model

type MeatProduct struct {
	ProductID   string  `json:"product_id" schema:"product_id"`
	Name        string  `json:"name" schema:"name"`
	Description string  `json:"description" schema:"description"`
	Price       float32 `json:"price" schema:"price"`
	Amount      int     `json:"amount" schema:"amount"`
	TypeID      int     `json:"type_id" schema:"type_id"`
	Barcode     string  `json:"barcode" schema:"barcode"`
	QRcode      string  `json:"qrcode" schema:"qrcode"`
	// TypeSellID  int     `json:"type_sell_id" schema:"type_sell_id"`
	MeatTypeID int `json:"meat_type_id" schema:"meat_type_id"`
}

package model

type PictureProduct struct {
	ID        int    `json:"id"`
	ProductID string `json:"product_id" schema:"product_id"`
	Path      string `json:"path" schema:"path"`
}

package model

type Product struct {
	Key              string  `json:"key"`
	TypeProductID    int     `json:"type_product_id"`
	Name             string  `json:"name"`
	Description      string  `json:"description"`
	Price            float32 `json:"price"`
	Amount           int     `json:"amount"`
	TypeID           int     `json:"type_id"`
	TypeDescpription string  `json:"type_description"`
	Barcode          string  `json:"barcode"`
	QRcode           string  `json:"qrcode"`
}

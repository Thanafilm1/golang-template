package model

type Bill struct {
	Id        int     `json:"id" schema:"id"`
	ReceiptAt int     `json:"receipt_at" schema:"receipt_at"`
	Vat       float32 `json:"vat" schema:"vat"`
	Total     float32 `json:"total" schema:"total"`
}

type BillOrder struct {
	BillID  int    `json:"bill_id" schema:"bill_id"`
	Barcode string `json:"barcode" schema:"barcode"`
	Amount  int    `json:"amount" schema:"amount"`
}

package model

type ProductType struct {
	ID    int    `json:"id" schema:"id"`
	Value string `json:"value" schema:"value"`
}

type MapProduct struct {
	Barcode       string `json:"barcode_key" schema:"barcode_key"`
	ProductID     string `json:"product_id" schema:"product_id"`
	TypeProductID int    `json:"type_product_id" schema:"type_product_id"`
}

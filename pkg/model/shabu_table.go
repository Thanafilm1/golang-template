package model

type ShabuTable struct {
	ID        int    `json:"id" schema:"id"`
	TableName string `json:"table_name" schema:"table_name"`
	Max       int    `json:"max" schema:"max"`
	Persons   int    `json:"persons" schema:"persons"`
	Adult     int    `json:"adult" schema:"adult"`
	Child     int    `json:"child" schema:"child"`
	CheckInAt int64  `json:"check_in_at" schema:"check_in_at"`
	StatusId  int    `json:"status_id" schema:"status_id"`
	Name      string `json:"name" schema:"name"`
	Phone     string `json:"phone" schema:"phone"`
	StartAt   int64  `json:"start_at" schema:"start_at"`
	EndAt     int64  `json:"end_at" schema:"end_at"`
}

type ShabuStatusTable struct {
	ID    int    `json:"id" schema:"id"`
	Value string `json:"value" schema:"value"`
}

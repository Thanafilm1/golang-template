package model

import jwt "github.com/dgrijalva/jwt-go"

// Token ...
type Token struct {
	ID        int    `json:"id"`
	Username  string `json:"username"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Phone     string `json:"phone"`
	RoleID    int    `json:"role_id"`
	Role      string `json:"role"`
	*jwt.StandardClaims
}

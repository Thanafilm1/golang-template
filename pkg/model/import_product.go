package model

type ImportProduct struct {
	ProductID   string  `json:"product_id" schema:"product_id"`
	Name        string  `json:"name" schema:"name,required"`
	Description string  `json:"description" schema:"description"`
	Price       float32 `json:"price" schema:"price,required"`
	MFDAt       int64   `json:"mfd_at" schema:"mfd_at,required"`
	EXPAt       int64   `json:"exp_at" schema:"exp_at,required"`
	Barcode     string  `json:"barcode" schema:"barcode"`
	QRcode      string  `json:"qrcode" schema:"qrcode"`
	Amount      int     `json:"amount" schema:"amount,required"`
	TypeID      int     `json:"type_id" schema:"type_id"`
}

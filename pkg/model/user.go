package model

// User ...
type User struct {
	ID        int    `json:"id"`
	Username  string `json:"username" schema:"username,required"`
	Password  string `json:"-" schema:"password,required"`
	FirstName string `json:"first_name" schema:"first_name"`
	LastName  string `json:"last_name" schema:"last_name"`
	Phone     string `json:"phone"`
	RoleID    int    `json:"role_id"`
	Role      string `json:"role"`
	CreatedAt int64  `json:"created_at"`
	UpdatedAt int64  `json:"updated_at"`
}

// Login ...
type Login struct {
	Token string `json:"token"`
	User  User   `json:"user"`
}

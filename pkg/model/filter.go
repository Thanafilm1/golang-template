package model

// Pagination ...
type Pagination struct {
	Page    int    `json:"page" schema:"page" default:"1"`
	Pages   int    `json:"pages" schema:"-" `
	Limit   int    `json:"limit" schema:"limit" default:"10"`
	Offset  int    `json:"offset" schema:"-"`
	Total   int    `json:"total" schema:"-"`
	Keyword string `json:"keyword" schema:"keyword"`
}

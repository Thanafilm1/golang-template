-- Adminer 4.7.8 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;

CREATE DATABASE `stock_imported` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `stock_imported`;

DROP TABLE IF EXISTS `bill`;
CREATE TABLE `bill` (
  `id` int NOT NULL COMMENT 'รหัสใบเสร็จ',
  `receipt_at` int NOT NULL COMMENT 'วันเวลาที่ออกบิล',
  `vat` float NOT NULL COMMENT 'ภาษี',
  `total` float NOT NULL COMMENT 'ราคา'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `bill_order`;
CREATE TABLE `bill_order` (
  `bill_id` int NOT NULL COMMENT 'รหัสรายการสินค้า',
  `barcode` int NOT NULL COMMENT 'รหัส barcode',
  `amount` int NOT NULL COMMENT 'จำนวน'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `import_product`;
CREATE TABLE `import_product` (
  `product_id` varchar(50) NOT NULL COMMENT 'รหัสสินค้า',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ชื่อสินค้า',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'รายละเอียด',
  `price` float NOT NULL COMMENT 'ราคาสินค้า/ชิ้น',
  `mfd_at` int NOT NULL COMMENT 'วันผลิต',
  `exp_at` int NOT NULL COMMENT 'วันหมดอายุ',
  `barcode` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'รหัส Barcode',
  `qrcode` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'รหัส Qrcode',
  `amount` int NOT NULL COMMENT 'จำนวน(ชิ้น)',
  `type_id` int NOT NULL COMMENT 'ประเภทสินค้า'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `map_product`;
CREATE TABLE `map_product` (
  `barcode_key` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'รหัส barcode',
  `product_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'รหัสสินค้า',
  `type_product_id` int NOT NULL COMMENT 'ประเภทสินค้า (นำเข้า/ชิ้นเนื้อ)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `meat_product`;
CREATE TABLE `meat_product` (
  `product_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'รหัสสินค้า',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ชื่อสินค้า',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'รายละเอียด',
  `price` float NOT NULL COMMENT 'ราคาสินค้า/ชิ้น',
  `amount` int NOT NULL COMMENT 'จำนวน(ชิ้น)',
  `type_id` int NOT NULL COMMENT 'ประเภทสินค้า(ขายปลีก, ชาบู, package)',
  `barcode` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'รหัส Barcode',
  `qrcode` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'รหัส Qrcode',
  `meat_type_id` int NOT NULL COMMENT 'รหัสประเภทของเนื้อ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `picture_product`;
CREATE TABLE `picture_product` (
  `id` int NOT NULL AUTO_INCREMENT,
  `product_id` varchar(50) NOT NULL COMMENT 'รหัสสินค้าหรือbarcode',
  `path` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ที่อยู่ภาพ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `product_outstock`;
CREATE TABLE `product_outstock` (
  `product_id` int NOT NULL COMMENT 'รหัสสินค้าหรือbarcode',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ชื่อสินค้า',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'รายละเอียด',
  `price` float NOT NULL COMMENT 'ราคาสินค้า/ชิ้น',
  `amount` int NOT NULL COMMENT 'จำนวน(ชิ้น)',
  `type_id` int NOT NULL COMMENT 'ประเภทสินค้า(ขายปลีก, ชาบู, package)',
  `barcode` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'รหัส Barcode',
  `qrcode` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'รหัส Qrcode',
  `type_sell_id` int NOT NULL COMMENT '(optional) รหัสชนิดการขาย',
  `meat_type_id` int NOT NULL COMMENT 'รหัสประเภทของเนื้อ',
  `status_outstock` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'สถานะสินค้า(กำลังคืน, คืนแล้ว, อยู่ในคลัง)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `product_type`;
CREATE TABLE `product_type` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'รหัสประเภท',
  `value` varchar(100) CHARACTER SET ucs2 NOT NULL COMMENT 'รายละเอียด',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `shabu_bill`;
CREATE TABLE `shabu_bill` (
  `table_id` int NOT NULL COMMENT 'เลขโต๊ะ',
  `receipt_at` timestamp NOT NULL COMMENT 'วันเวลาที่ออกบิล',
  `vat` float NOT NULL COMMENT 'ภาษี',
  `total` float NOT NULL COMMENT 'ราคารวม'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `shabu_list_orders`;
CREATE TABLE `shabu_list_orders` (
  `order_id` int NOT NULL COMMENT 'เลขออเดอร์',
  `barcode_id` timestamp NOT NULL COMMENT 'รหัสสินค้า',
  `amount` int NOT NULL COMMENT 'จำนวน',
  `price` float NOT NULL COMMENT 'ราคา',
  `at` timestamp NOT NULL COMMENT 'เวลาที่สั่ง',
  `status` int NOT NULL COMMENT 'สถานะสินค้า'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `shabu_order`;
CREATE TABLE `shabu_order` (
  `id` int NOT NULL COMMENT 'เลขออเดอร์',
  `table_id` int NOT NULL COMMENT 'เลขโต๊ะ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `shabu_promotion`;
CREATE TABLE `shabu_promotion` (
  `id` int NOT NULL COMMENT 'เลขโปรโมชัน',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ชื่อโปรโมชัน',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'รายละเอียด'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `shabu_status_table`;
CREATE TABLE `shabu_status_table` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'รหัสสถานะ',
  `value` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'รายละเอียด',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `shabu_table`;
CREATE TABLE `shabu_table` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'เลขโต๊ะ',
  `table_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ชื่ื่อโต๊ะ',
  `max` int NOT NULL COMMENT 'จำนวนคนต่อโต๊ะ',
  `persons` int NOT NULL DEFAULT '0' COMMENT 'จำนวนคนทั้งหมด',
  `adult` int NOT NULL DEFAULT '0' COMMENT 'ผู้ใหญ่',
  `child` int NOT NULL DEFAULT '0' COMMENT 'เด็ก',
  `check_in_at` int NOT NULL DEFAULT '0' COMMENT 'เวลาจอง',
  `status_id` int NOT NULL DEFAULT '1' COMMENT 'สถานะโต๊ะ',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'ชื่อคนจอง',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'เบอร์โทร',
  `start_at` int NOT NULL DEFAULT '0' COMMENT 'เวลาเข้า',
  `end_at` int NOT NULL DEFAULT '0' COMMENT 'เวลาออก',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `shabu_table_promotion`;
CREATE TABLE `shabu_table_promotion` (
  `table_id` int NOT NULL COMMENT 'เลขโต๊ะ',
  `promotion_id` int NOT NULL COMMENT 'เลขโปรโมชัน'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 2021-01-23 10:39:28

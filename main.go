package main

import (
	"store_beef_backend/pkg/protocol/rest"
)

func main() {
	rest.RunServer("3000")
}
